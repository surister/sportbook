# SportsBook
A sports data scraping and analysis tool

This project is in its very early days and currently only supports football (soccer).

Fearure list:

* Import leagues and fixtures from one of two available online sources.

* Run manual comparison of two teams from any loaded leagues generating (somewhat inaccurate) predictions.

* Run predictions on all loaded fixtures (more accurate as the teams are guaranteed to be from the same leagues).

* Filter predictions to show games where specific requirements are met (eg. prediction of home side winning by 2 goals).

* Filter filtered predictions further with other filters.

* Filter predictions using special filters (either produced by guest contributors or specially designed filters for specific bet types).

* Display filtered predictions and all predictions on screen.

* Change the range of dates or games the predictions cover.

* Produce a spreadsheet of all predictions or filtered predictions with a wealth of current stats for each team in each prediction.

* Export currently loaded league data to a JSON file.

* Import the league data from a JSON file.

The project is growing fairly quickly. I'd love to hear what your thoughts are and even keep you up to date with new features if you like. Join the Slack group here if you're interested:

https://join.slack.com/t/sportsbookgroup/shared_invite/enQtNDQ5MDMyMTIwNzA2LWRhY2YxOGY0NWYzNTE2ZDMyNGI3NWQzMTk0YTkxODMyNzEyM2UyMjlkMTVlYmU3NDMzNTVlNWVmZDM4YTNjMjE

It'd be great to hear from you so please pop in and say hi!
